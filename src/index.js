import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'

class Input extends React.Component {
    render() {
        let{result} = this.props;
        return (
            <div>{result}</div>
        )
    }
}

class Buttons extends React.Component {
    constructor(props){
        super(props);
        this.state = {buttons:Array(18).fill(1)}
    }
    render() {
        return (
            <div>
                <div className="row top">
                    <button name='C' onClick={e => this.props.onClick(e.target.name)}>C</button>
                    <button name='/' onClick={e => this.props.onClick(e.target.name)}>/</button>

                </div>
                <div className="row">
                    <button name='7' onClick={e => this.props.onClick(e.target.name)}>7</button>
                    <button name='8' onClick={e => this.props.onClick(e.target.name)}>8</button>
                    <button name='9' onClick={e => this.props.onClick(e.target.name)}>9</button>
                    <button name='*' onClick={e => this.props.onClick(e.target.name)}>*</button>
                </div>
                <div className="row">
                    <button name='4' onClick={e => this.props.onClick(e.target.name)}>4</button>
                    <button name='5' onClick={e => this.props.onClick(e.target.name)}>5</button>
                    <button name='6' onClick={e => this.props.onClick(e.target.name)}>6</button>
                    <button name='-' onClick={e => this.props.onClick(e.target.name)}>-</button>
                </div>
                <div className="row">
                    <button name='1' onClick={e => this.props.onClick(e.target.name)}>1</button>
                    <button name='2' onClick={e => this.props.onClick(e.target.name)}>2</button>
                    <button name='3' onClick={e => this.props.onClick(e.target.name)}>3</button>
                    <button name='+' onClick={e => this.props.onClick(e.target.name)}>+</button>
                </div>
                <div className="row">
                    <button name='+/-' onClick={e => this.props.onClick(e.target.name)}>+/-</button>
                    <button name='0' onClick={e => this.props.onClick(e.target.name)}>0</button>
                    <button name='.' onClick={e => this.props.onClick(e.target.name)}>.</button>
                    <button name='=' onClick={e => this.props.onClick(e.target.name)}>=</button>
                </div>
            </div>
        )
    }
}

class Calculator extends React.Component {
    constructor(props){
        super(props)
        this.state = {result:''}
    }
    onClick = button => {
        if(button === "="){
            this.calculate()
        } else if(button ==="C"){
            this.reset()
        } else {
            this.setState({result: this.state.result + button})
        }
    };

    calculate = () => {
        let calcResult = this.state.result;

        try{
            this.setState({result: (eval(calcResult || '') + '')})
        } catch (e) {
            this.setState({result: 'error'})
        }
    };

    reset = () => {
        this.setState({result: ''})
    };
    render() {
        return (
            <div className="calculator">
                <Input result = {this.state.result}/>
                <Buttons onClick = {this.onClick}/>
            </div>
        )
    }
}
ReactDOM.render(
    <Calculator/>, document.getElementById('root')
);
